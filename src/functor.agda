{-
Copyright 2020 Tex Schönlank

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module functor where

  open import Data.Unit using (⊤ ; tt)
  open import Data.Empty using (⊥)
  open import sorry

  open import Algebra using (IsMonoid; Op₂)
  open import Relation.Binary using (Rel)

  import Relation.Binary.PropositionalEquality as Eq
  open Eq using (_≡_; refl; trans; sym; cong; cong₂; cong-app; subst)
  open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

  -- We start by defining the universe of regular functors. It
  -- contains the identity functor and the constant functor A for any
  -- type A, and is closed under co- and products and concatenation.
  data Functor : Set where
    |Id| : Functor
    |V| : Functor
    |U| : Functor
    _|+|_ : Functor -> Functor -> Functor
    _|x|_ : Functor -> Functor -> Functor
    _|∘|_ : Functor -> Functor -> Functor

  infixr 50 _|+|_
  infixr 60 _|x|_

  -- Then we define two type constructors: co- and products.
  data _⊕_ (A B : Set) : Set where
    inl : A -> A ⊕ B
    inr : B -> A ⊕ B

  record _⊗_ (A B : Set) : Set where
    constructor _,_
    field
      fst : A
      snd : B

  infixr 50 _⊕_
  infixr 60 _⊗_
  infixr 70 _,_

  fst : {A B : Set} -> A ⊗ B -> A
  fst (a , b) = a

  snd : {A B : Set} -> A ⊗ B -> B
  snd (a , b) = b

  -- We define how a functor F maps types to types.
  [_] : (F : Functor) -> Set -> Set
  [ |Id| ] A = A
  [ |V| ] A = ⊥
  [ |U| ] A = ⊤
  [ F1 |+| F2 ] A = [ F1 ] A ⊕ [ F2 ] A
  [ F1 |x| F2 ] A = [ F1 ] A ⊗ [ F2 ] A
  [ F1 |∘| F2 ] A = [ F1 ]([ F2 ] A)

  -- From this, we can define how a functor maps a morphism.
  map : (F : Functor) -> {A B : Set} -> (A -> B) ->
    ([ F ] A -> [ F ] B)
  map |Id| m fa = m fa
  map |V| m ()
  map |U| m fa = fa
  map (FL |+| FR) m (inl fla) = inl (map FL m fla)
  map (FL |+| FR) m (inr fra) = inr (map FR m fra)
  map (FL |x| FR) m (fla , fra) = map FL m fla , map FR m fra
  map (FL |∘| FR) m flfra = map FL (map FR m) flfra

  -- A property of map is that if you map the identity function on
  -- something, you get the original thing.
  map-id : {A : Set} -> (F : Functor) -> (m : A -> A) ->
    (fa : [ F ] A) -> (m-id : (a : A) -> m a ≡ a) -> map F m fa ≡ fa
  map-id |Id| m a m-id = m-id a
  map-id |U| m tt m-id = refl
  map-id |V| m ()
  map-id (FL |+| FR) m (inl fla) m-id =
    cong inl (map-id FL m fla m-id)
  map-id (FL |+| FR) m (inr fra) m-id =
    cong inr (map-id FR m fra m-id)
  map-id (FL |x| FR) m (fla , fra) m-id =
    cong₂ _,_ (map-id FL m fla m-id) (map-id FR m fra m-id)
  map-id (FL |∘| FR) m flfra m-id =
    sorry

  -- We define the fixpoint of any given functor.
  data μ_ (F : Functor) : Set where
    <_> : [ F ](μ F) -> (μ F)

  -- For convenience, we also define these two functions: the inverse
  -- of the fixpoint constructor and function concatenation
  <_>^-1 : {F : Functor} -> (muf : μ F) -> [ F ](μ F)
  < muf >^-1 with muf
  ... | < fmuf > = fmuf

  _∘_ : {A B C : Set} -> (B -> C) -> (A -> B) -> (A -> C)
  g ∘ f = λ a -> g (f a)
  infixr 20 _∘_

  -- Here, we have the function mapcata, which turns an algebra into
  -- the relevant catamorphism and then maps another functor on this
  -- catamorphism. This is done for termination checking purposes.
  {-# TERMINATING #-}
  mapcata : (F G : Functor) -> {X : Set} -> ([ G ] X -> X) ->
    ([ F ](μ G) -> [ F ] X)
  mapcata |Id| G α < c > = α (mapcata G G α c)
  mapcata |V| G α ()
  mapcata |U| G α c = c
  mapcata (FL |+| FR) G α (inl c) = inl (mapcata FL G α c)
  mapcata (FL |+| FR) G α (inr c) = inr (mapcata FR G α c)
  mapcata (FL |x| FR) G α (cl , cr) =
    mapcata FL G α cl , mapcata FR G α cr
  mapcata (FL |∘| FR) G α c = map FL (mapcata FR G α) c

  -- We can of course also define cata itself by letting the second
  -- functor be the identity functor.
  cata : (F : Functor) -> {X : Set} -> ([ F ] X -> X) -> (μ F -> X)
  cata F α = mapcata |Id| F α

  -- We define the notion of congruence of two structures over the
  -- same functor.
  Cong : {A B : Set} -> (p : A -> B -> Set) ->
      (F : Functor) -> (fa : [ F ] A) -> (fb : [ F ] B) -> Set
  Cong p |Id| fa fb = p fa fb
  Cong p |U| tt tt = ⊤
  Cong p (FL |+| FR) (inl fla) (inl flb) = Cong p FL fla flb
  Cong p (FL |+| FR) (inl fla) (inr frb) = ⊥
  Cong p (FL |+| FR) (inr fra) (inl flb) = ⊥
  Cong p (FL |+| FR) (inr fra) (inr frb) = Cong p FR fra frb
  Cong p (FL |x| FR) (fla , fra) (flb , frb) =
    Cong p FL fla flb ⊗ Cong p FR fra frb
  Cong p (FL |∘| FR) flfra flfrb = Cong (Cong p FR) FL flfra flfrb

  -- We also define the useful shorthand congt for congruent using the
  -- trivially satisfied predicate.
  triv : {A B : Set} -> A -> B -> Set
  triv a b = ⊤

  Congt : {A B : Set} -> (F : Functor) ->
    (fa : [ F ] A) -> (fb : [ F ] B) -> Set
  Congt = Cong triv

  -- Below are the properties describing that Congt is an equivalence
  -- relation. We are not interested in proving this.
  congt-trans : {A B C : Set} -> (F : Functor) ->
    {fa : [ F ] A} -> {fb : [ F ] B} -> {fc : [ F ] C} ->
    Congt F fa fb -> Congt F fb fc -> Congt F fa fc
  congt-trans = sorry

  congt-refl : {A : Set} -> (F : Functor) -> (fa : [ F ] A) ->
    Congt F fa fa
  congt-refl = sorry

  congt-symm : {A B : Set} -> (F : Functor) ->
    {fa : [ F ] A} -> {fb : [ F ] B} ->
    Congt F fa fb -> Congt F fb fa
  congt-symm = sorry

  -- We define a function which maps a predicate and a functor
  -- structure onto the type of all proofs that every "element" of the
  -- structure satisfies the predicate.
  all : {A : Set} -> (F : Functor) -> (p : A -> Set) -> [ F ] A -> Set
  all |Id| p a = p a
  all |U| p tt = ⊤
  all |V| p ()
  all (FL |x| FR) p (fla , fra) = all FL p fla ⊗ all FR p fra
  all (FL |+| FR) p (inl fla) = all FL p fla
  all (FL |+| FR) p (inr fra) = all FR p fra
  all (FL |∘| FR) p flfra = all FL (all FR p) flfra

  all₂ : {A B : Set} -> (F : Functor) -> (p : A -> B -> Set) ->
    [ F ] (A ⊗ B) -> Set
  all₂ F p = all F (λ { (a , b) → p a b })

  -- By definition of the trivially true property, it holds for every
  -- element pair of any structure.
  all₂-triv : {A B : Set} -> (F : Functor) -> (faxb : [ F ](A ⊗ B)) ->
    all₂ F triv faxb
  all₂-triv F faxb = sorry

  all-implies : {A : Set} -> (F : Functor) -> (p q : A -> Set) ->
    ((a : A) -> p a -> q a) -> (fa : [ F ] A) ->
    all F p fa -> all F q fa
  all-implies |Id| p q i a allfa = i a allfa
  all-implies |U| p q i tt allfa = tt
  all-implies |V| p q i ()
  all-implies (FL |x| FR) p q i (fla , fra) allfa =
    all-implies FL p q i fla (fst allfa) ,
    all-implies FR p q i fra (snd allfa)
  all-implies (FL |+| FR) p q i (inl fla) = all-implies FL p q i fla
  all-implies (FL |+| FR) p q i (inr fra) = all-implies FR p q i fra
  all-implies {A} (FL |∘| FR) p q i flfra allfa =
    all-implies FL (all FR p) (all FR q) ih flfra allfa
    where
      ih = all-implies FR p q i

  all-map : {A B : Set} -> (F : Functor) -> (p : B -> Set) ->
    (m : A -> B) -> (fa : [ F ] A) ->
    all F (λ x → p (m x)) fa -> all F p (map F m fa)
  all-map |Id| p m a allfa = allfa
  all-map |U| p m tt allfa = tt
  all-map |V| p m ()
  all-map (FL |x| FR) p m (fla , fra) allfa =
    all-map FL p m fla (fst allfa) , all-map FR p m fra (snd allfa)
  all-map (FL |+| FR) p m (inl fla) allfa = all-map FL p m fla allfa
  all-map (FL |+| FR) p m (inr fra) allfa = all-map FR p m fra allfa
  all-map (FL |∘| FR) p m flfra allfa =
    all-map FL (all FR p) (map FR m) flfra
      (all-implies FL (all FR (λ x → p (m x)))
        (λ fra → all FR p (map FR m fra)) ih flfra allfa)
    where ih = all-map FR p m

  all₂-map : {A B C : Set} -> (F : Functor) -> (p : B -> C -> Set) ->
    (m : A -> B ⊗ C) -> (fa : [ F ] A) ->
    all F (λ {a → p (fst (m a)) (snd (m a))}) fa ->
    all₂ F p (map F m fa)
  all₂-map {A} {B} {C} F p m fa allfa = all-map F p' m fa allfa
    where
      p' : (B ⊗ C) -> Set
      p' (b , c) = p b c

  -- We define the function unzip, which takes a structure of A,B
  -- tuples and returns an A structure and a B structure
  unzip : {A B : Set} -> (F : Functor) -> (faxb : [ F ] (A ⊗ B)) ->
    [ F ] A ⊗ [ F ] B
  unzip |Id| axb = fst axb , snd axb
  unzip |U| axb = tt , tt
  unzip (FL |x| FR) (flaxb , fraxb) =
    (fst (unzip FL flaxb) , fst (unzip FR fraxb)) ,
    (snd (unzip FL flaxb) , snd (unzip FR fraxb))
  unzip (FL |+| FR) (inl flaxb) =
    inl (fst (unzip FL flaxb)) ,
    inl (snd (unzip FL flaxb))
  unzip (FL |+| FR) (inr fraxb) =
    inr (fst (unzip FR fraxb)) ,
    inr (snd (unzip FR fraxb))
  unzip (FL |∘| FR) flfraxb =
    unzip FL (map FL (unzip FR) flfraxb)

  -- We prove that the two resulting structures of unzip are always
  -- congruent with each other.
  unzip-cong : {A B : Set} -> (F : Functor) ->
    (p : A -> B -> Set) -> (faxb : [ F ] (A ⊗ B)) ->
    all₂ F p faxb ->
    Cong p F (fst (unzip F faxb)) (snd (unzip F faxb))
  unzip-cong |Id| p (a , b) allp = allp
  unzip-cong |U| p tt allp = tt
  unzip-cong (FL |+| FR) p (inl (flaxb)) allp =
    unzip-cong FL p flaxb allp
  unzip-cong (FL |+| FR) p (inr (fraxb)) allp =
    unzip-cong FR p fraxb allp
  unzip-cong (FL |x| FR) p (flaxb , fraxb) allp =
    unzip-cong FL p flaxb (fst allp) ,
    unzip-cong FR p fraxb (snd allp)
  unzip-cong (FL |∘| FR) p flfraxb allp =
    unzip-cong FL (Cong p FR) (map FL (unzip FR) flfraxb)
      (all₂-map FL (Cong p FR) (unzip FR) flfraxb
        (all-implies
          FL
          (all₂ FR p)
          (λ z → Cong p FR (fst (unzip FR z)) (snd (unzip FR z)))
          (unzip-cong FR p)
          flfraxb
          allp))


  -- We also define the convenience function zip, which zips an A
  -- structure and a B structure into one A,B tuple structure. We need
  -- to be certain that the two given structures are congruent though,
  -- so we require a proof of type (Cong triv fa fb). Here triv is the
  -- trivially true predicate: we don't require any special properties
  -- on the pairs.
  zip : {A B C : Set} -> (F : Functor) -> (p : A -> B -> Set) ->
    (z : (a : A) -> (b : B) -> p a b -> C) ->
    (fa : [ F ] A) -> (fb : [ F ] B) ->
    Cong p F fa fb -> [ F ] C
  zip |Id| p z a b pab = z a b pab
  zip |U| p z tt tt c = tt
  zip (FL |+| FR) p z (inl fla) (inl flb) c =
    inl (zip FL p z fla flb c)
  zip (FL |+| FR) p z (inr fra) (inr frb) c =
    inr (zip FR p z fra frb c)
  zip (FL |x| FR) p z (fla , fra) (flb , frb) (cl , cr) =
    zip FL p z fla flb cl , zip FR p z fra frb cr
  zip (FL |∘| FR) p z flfra flfrb c =
    zip FL (Cong p FR) (zip FR p z) flfra flfrb c

  -- We define what it means for a Set to have a monoid defined on it.
  record HasMonoid (A : Set) : Set₁ where
    field
      _∙_ : Op₂ A
      ε : A
      proof : IsMonoid _≡_ _∙_ ε

    infixl 80 _∙_

  -- Stupid helper function we don't strictly need but I do like.
  first : {A B C : Set} -> (A -> C) -> (A ⊗ B) -> (C ⊗ B)
  first f (a , b) = f a , b

  -- Given all these things, we finally do what we wanted to do in the
  -- module: we make an algorithm that performs a left-scan on
  -- structures of functors in the universe. We have to mix it into a
  -- mutually recursive block together with a proof that the result of
  -- lscan (ignoring the bonus element on the right) is Congt with the
  -- original.
  lscan : {A : Set} -> {{_ : HasMonoid A}} -> (F : Functor) ->
    [ F ] A -> [ F ] A ⊗ A

  lscan-congt : {A : Set} -> {{_ : HasMonoid A}} -> (F : Functor) ->
    (fa : [ F ] A) -> Congt F fa (fst (lscan F fa))

  lscan |Id| a = ε , a where open HasMonoid {{...}}
  lscan |U| tt = tt , ε where open HasMonoid {{...}}
  lscan |V| ()
  lscan (FL |+| FR) (inl fla) = first inl (lscan FL fla) where
    open HasMonoid {{...}}
  lscan (FL |+| FR) (inr fra) = first inr (lscan FR fra) where
    open HasMonoid {{...}}
  lscan (FL |x| FR) (fla , fra) =
    let (resfla , bonfla) = lscan FL fla
        (resfra , bonfra) = lscan FR fra
    in
      (resfla , map FR (λ x → bonfla ∙ x) resfra) , (bonfla ∙ bonfra)
    where open HasMonoid {{...}}
  lscan (FL |∘| FR) flfra =
    let (flresflfra , flbonflfra) = unzip FL (map FL (lscan FR) flfra)
        (resbon , bonbon) = lscan FL flbonflfra
    in zip FL triv (λ a fra _ → map FR (a ∙_) fra) resbon flresflfra l , bonbon
    where open HasMonoid {{...}}
          l0 : Congt FL (fst (unzip FL (map FL (lscan FR) flfra)))
                        (snd (unzip FL (map FL (lscan FR) flfra)))
          l0 = unzip-cong FL triv
                 (map FL (lscan FR) flfra)
                 (all₂-triv FL (map FL (lscan FR) flfra))
          l1 : Congt FL
                 (snd (unzip FL (map FL (lscan FR) flfra)))
                 (fst (lscan FL (snd (unzip FL (map FL (lscan FR) flfra)))))
          l1 = lscan-congt FL (snd (unzip FL (map FL (lscan FR) flfra)))
          l = congt-symm FL (congt-trans FL l0 l1)
  lscan-congt |Id| a = tt
  lscan-congt |U| tt = tt
  lscan-congt |V| ()
  lscan-congt (FL |+| FR) (inl fla) = lscan-congt FL fla
  lscan-congt (FL |+| FR) (inr fra) = lscan-congt FR fra
  lscan-congt (FL |x| FR) (fla , fra) =
    lscan-congt FL fla ,
    congt-trans FR
      {fra}
      {(snd (fla , fra))}
      {(snd (fst (lscan (FL |x| FR) (fla , fra))))}
      (congt-refl FR fra)
      sorry -- By recursion on FR lifted over snd.
  lscan-congt (FL |∘| FR) flfra = sorry
