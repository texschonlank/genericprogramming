{-
Copyright 2020 Tex Schönlank

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module flatten where

  open import Data.List
    hiding ([_] ; concat ; _++_)
    renaming (map to map-list)
  open import functor
  open import Data.Nat
  open import sorry

  open import ext

  import Relation.Binary.PropositionalEquality as Eq
  open Eq using (_≡_; refl; trans; sym; cong; cong₂; cong-app; subst)
  open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

  import Algebra.Definitions
  import Algebra.Structures

  _++_ : {A : Set} -> List A -> List A -> List A
  _++_ [] l2 = l2
  _++_ (a1 ∷ l1) l2 = a1 ∷ _++_ l1 l2

  join : {A : Set} -> List (List A) -> List A
  join [] = []
  join (as ∷ ass) = as ++ join ass

  flatten : {A : Set} -> (F : Functor) -> (fa : [ F ] A) -> List A
  flatten |Id| a = a ∷ []
  flatten |U| tt = []
  flatten |V| ()
  flatten (FL |x| FR) (fla , fra) = flatten FL fla ++ flatten FR fra
  flatten (FL |+| FR) (inl fla) = flatten FL fla
  flatten (FL |+| FR) (inr fra) = flatten FR fra
  flatten (FL |∘| FR) (flfra) =
    join (flatten FL (map FL (flatten FR) flfra))

  -- We must first prove an easy property of map-list
  map-list-append : {A B : Set} -> (m : A -> B) ->
    (as1 as2 : List A) ->
    map-list m (as1 ++ as2) ≡ map-list m as1 ++ map-list m as2
  map-list-append m [] as2 = refl
  map-list-append m (a1 ∷ as1) as2 =
    begin
      map-list m ((a1 ∷ as1) ++ as2)
    ≡⟨ refl ⟩
      map-list m (a1 ∷ (as1 ++ as2))
    ≡⟨ refl ⟩
      m a1 ∷ map-list m (as1 ++ as2)
    ≡⟨ cong₂ _∷_ refl (map-list-append m as1 as2) ⟩
      m a1 ∷ (map-list m as1 ++ map-list m as2)
    ≡⟨ refl ⟩
      map-list m (a1 ∷ as1) ++ map-list m as2
    ∎

  -- A joint property of map, map-list, and flatten is that
  -- (flatten F) ∘ (map F m) and (map-list m) ∘ (flatten F) commute on
  -- every F structure.
  map-flat-comm :
    {A B : Set} -> (F : Functor) -> (m : A -> B) -> (fa : [ F ] A) ->
    flatten F (map F m fa) ≡ map-list m (flatten F fa)
  map-flat-comm |Id| m a = refl
  map-flat-comm |U| m tt = refl
  map-flat-comm |V| m ()
  map-flat-comm (FL |+| FR) m (inl fla) = map-flat-comm FL m fla
  map-flat-comm (FL |+| FR) m (inr fra) = map-flat-comm FR m fra
  map-flat-comm (FL |x| FR) m (fla , fra) =
    begin
      flatten (FL |x| FR) (map (FL |x| FR) m (fla , fra))
    ≡⟨ refl ⟩
      flatten FL (map FL m fla) ++ flatten FR (map FR m fra)
    ≡⟨ cong₂ _++_ (map-flat-comm FL m fla) (map-flat-comm FR m fra) ⟩
      map-list m (flatten FL fla) ++ map-list m (flatten FR fra)
    ≡⟨ sym (map-list-append m (flatten FL fla) (flatten FR fra)) ⟩
      map-list m (flatten FL fla ++ flatten FR fra)
    ≡⟨ cong (map-list m) refl ⟩
      map-list m (flatten (FL |x| FR) (fla , fra))
    ∎
  map-flat-comm (FL |∘| FR) m flfra = sorry

  scan-list : {A : Set} -> {{_ : HasMonoid A}} -> List A -> List A ⊗ A
  scan-list [] = [] , ε where open HasMonoid {{...}}
  scan-list (a ∷ as) = let (res , bon) = scan-list as in
    (ε ∷ Data.List.map (λ x → a ∙ x) res) , a ∙ bon
    where open HasMonoid {{...}}

  scan-list-property : {A : Set} -> {{hm : HasMonoid A}} ->
    (as bs : List A) ->
    scan-list (as ++ bs) ≡
      (fst (scan-list as) ++
        Data.List.map (HasMonoid._∙_ hm (snd (scan-list as)))
                      (fst (scan-list bs)))
    , HasMonoid._∙_ hm (snd (scan-list as)) (snd (scan-list bs))
  scan-list-property [] bs = sorry -- Rather obvious
  scan-list-property {{hm = hm'}} (a ∷ as) bs = let
    ass = Algebra.Structures.IsSemigroup.assoc
            (Algebra.Structures.IsMonoid.isSemigroup
              (HasMonoid.proof hm'))
    l1 : (a ∙_) (snd (scan-list (as ++ bs)))
         ≡ a ∙ snd (scan-list as) ∙ snd (scan-list bs)
    l1 = begin -- Use associativity of ∙ and recursion on as bs.
      (a ∙_) (snd (scan-list (as ++ bs)))
        ≡⟨ cong (a ∙_) (cong snd (scan-list-property as bs))⟩
      (a ∙_) (snd (scan-list as) ∙ snd (scan-list bs))
        ≡⟨ sym (ass a (snd (scan-list as)) (snd (scan-list bs))) ⟩
      a ∙ snd (scan-list as) ∙ snd (scan-list bs)
        ∎
    l2 : fst (scan-list (as ++ bs))
         ≡ fst (scan-list as) ++ map-list (snd (scan-list as) ∙_)
                                          (fst (scan-list bs))
    l2 = begin
        fst (scan-list (as ++ bs))
      ≡⟨ cong fst (scan-list-property as bs) ⟩
        fst (scan-list as) ++ map-list (snd (scan-list as) ∙_)
                                       (fst (scan-list bs))
      ∎
    l3 : map-list (a ∙_) (map-list (snd (scan-list as) ∙_)
                                   (fst (scan-list bs)))
         ≡ map-list ((a ∙_) ∘ (snd (scan-list as) ∙_))
                    (fst (scan-list bs))
    l3 = sorry -- Very easy since extensionality is postulated.
    l4 : ((a ∙_) ∘ (snd (scan-list as) ∙_))
         ≡ ((a ∙ snd (scan-list as)) ∙_)
    l4 = let ssclas = snd (scan-list as) ; open HasMonoid {{...}} in
      ext λ { a' → sym (ass a ssclas a') }
    l5 : map-list (a ∙_) (fst (scan-list (as ++ bs)))
         ≡
         (map-list (a ∙_) (fst (scan-list as))
          ++
          map-list ((a ∙ snd (scan-list as)) ∙_)
                   (fst (scan-list bs))
         )
    l5 = begin
         map-list (a ∙_) (fst (scan-list (as ++ bs)))
       ≡⟨ cong (map-list (a ∙_)) l2 ⟩
         map-list (a ∙_) (fst (scan-list as)
         ++ map-list (snd (scan-list as) ∙_)
                     (fst (scan-list bs)))
       ≡⟨ map-list-append (a ∙_) (fst (scan-list as))
            (map-list (snd (scan-list as) ∙_) (fst (scan-list bs))) ⟩
         map-list (a ∙_) (fst (scan-list as))
         ++ map-list (a ∙_) (map-list (snd (scan-list as) ∙_)
                                      (fst (scan-list bs)))
       ≡⟨ cong₂ _++_ refl l3 ⟩
         map-list (a ∙_) (fst (scan-list as))
         ++ map-list ((a ∙_) ∘ (snd (scan-list as) ∙_))
                     (fst (scan-list bs))
       ≡⟨ cong₂ _++_ refl
                (cong (λ {x → map-list x (fst (scan-list bs))}) l4) ⟩
         map-list (a ∙_) (fst (scan-list as))
         ++
         map-list ((a ∙ snd (scan-list as)) ∙_)
                  (fst (scan-list bs))
       ∎
    in begin
      (ε ∷  map-list (a ∙_) (fst (scan-list (as ++ bs))))
      , (a ∙_) (snd (scan-list (as ++ bs)))
    ≡⟨ cong₂ _,_ (cong (ε ∷_) l5 ) l1 ⟩
      (ε ∷
       (map-list (a ∙_) (fst (scan-list as))
        ++
        map-list ((a ∙ snd (scan-list as)) ∙_)
                 (fst (scan-list bs))))
      ,
      a ∙ snd (scan-list as) ∙ snd (scan-list bs)
    ∎ where open HasMonoid {{...}}

  module example where
    open import Data.Bool using (Bool ; true ; false)
    open import datatypes
    open import Data.Unit using (tt)

    f = RPow (|Id| |+| RVec 3) 2
    ex : [ f ] Bool
    ex = inr (inl true ,
              (inr (true ,
                    (false ,
                     (false , tt))) ,
               (inr (false , (true , (false , tt))) ,
                tt)))
    -- (flatten f ex) should evaluate to
    -- true true false false false true false

{-  flatten-lscan-first-elem : {A : Set} -> {{hm : HasMonoid A}} -> {a : A} -> {as : List A} ->
    (F : Functor) -> (fa : [ F ] A) -> flatten F fa ≡ a ∷ as ->
    flatten F (fst (lscan F fa)) ≡ HasMonoid.ε hm ∷ _
-}
