{-
Copyright 2020 Tex Schönlank

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module datatypes where

  open import functor
  open import Data.Nat using (ℕ; suc)

  -- ### Type constructors that don't live in the universe
  -- The `S' stands for Set, because it is a Set definition and not a
  -- data definition.

  -- Right- and left-growing lists
  data SRList (A : Set) : Set where
    RNil : SRList A
    RCons : A -> SRList A -> SRList A

  data SLList (A : Set) : Set where
    LNil : SLList A
    LCons : SLList A -> A -> SLList A

  -- Top-down and bottom-up trees of fixed depth (i.e. they have a
  -- fixed number of applications of the functor).
  data SRPow (F : Functor) (A : Set) : (n : ℕ) -> Set where
    RLeaf : A -> SRPow F A 0
    RBranch : {n : ℕ} -> [ F ](SRPow F A n) -> SRPow F A (suc n)

  data SLPow (F : Functor) (A : Set) : (n : ℕ) -> Set where
    LLeaf : A -> SLPow F A 0
    LBranch : {n : ℕ} -> (SLPow F ([ F ] A) n) -> SLPow F A (suc n)

  -- ### Type constructors that /do/ live in the universe

  -- I define the (L|R)(Vec|Pow) types to be Functors in the universe.
  RVec : ℕ -> Functor
  RVec ℕ.zero = |U|
  RVec (ℕ.suc n) = |Id| |x| RVec n

  LVec : ℕ -> Functor
  LVec ℕ.zero = |U|
  LVec (ℕ.suc n) = LVec n |x| |Id|

  RPow : Functor -> ℕ -> Functor
  RPow F ℕ.zero = |Id|
  RPow F (ℕ.suc n) = F |∘| RPow F n

  LPow : Functor -> ℕ -> Functor
  LPow F ℕ.zero = |Id|
  LPow F (ℕ.suc n) = LPow F n |∘| F

  -- We define the notion of left-scanability as a record. We do this,
  -- because there might be properties other than the lscan function
  -- itself that we might need within the definition of the lscan
  -- function, and this might be more elegant than a mutual block.
{-
  record LScan (F : Functor) (A : Set) : Set where
    field
      lscan : [ F ] A -> [ F ] A ⊗ A
-}
