{-
Copyright 2020 Tex Schönlank

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module result where

  open import Data.Unit using (⊤ ; tt)

  open import functor
  open import flatten
  open import sorry

  open import Algebra using (IsMonoid)
  open import Data.Product using (proj₁ ; proj₂)

  open import Data.List
    hiding ([_] ; concat ; _++_ ; zip)
    renaming (map to map-list)

  import Relation.Binary.PropositionalEquality as Eq
  open Eq using (_≡_; refl; trans; sym; cong; cong₂; cong-app; subst)
  open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

  open import ext

  data Singleton {a} {A : Set a} (x : A) : Set a where
    _with≡_ : (y : A) → x ≡ y → Singleton x

  inspect : ∀ {a} {A : Set a} (x : A) → Singleton x
  inspect x = x with≡ refl

  -- We define and prove the final two lemmas that we would like to
  -- have.
  lscan-correct : {A : Set} -> (F : Functor) -> (fa : [ F ] A) ->
    {{_ : HasMonoid A}} ->
    first (flatten F) (lscan F fa) ≡ scan-list (flatten F fa)
  lscan-correct |Id| a = cong (λ x → (ε ∷ []) , x) (sym (rightId a))
    where
      open import Data.Product using (proj₂)
      open HasMonoid {{...}}
      rightId = proj₂ (IsMonoid.identity proof)
  lscan-correct |U| tt = refl
  lscan-correct |V| ()
  lscan-correct (FL |+| FR) (inl fla) = begin
    first (flatten (FL |+| FR)) (lscan (FL |+| FR) (inl fla))
      ≡⟨⟩
    flatten FL (fst (lscan FL fla)) , snd (lscan FL fla)
      ≡⟨ lscan-correct FL fla ⟩
    scan-list (flatten FL fla)
      ≡⟨⟩
    scan-list (flatten (FL |+| FR) (inl fla))
      ∎
  lscan-correct (FL |+| FR) (inr fra) = begin
    first (flatten (FL |+| FR)) (lscan (FL |+| FR) (inr fra))
      ≡⟨⟩
    flatten FR (fst (lscan FR fra)) , snd (lscan FR fra)
      ≡⟨ lscan-correct FR fra ⟩
    scan-list (flatten FR fra)
      ≡⟨⟩
    scan-list (flatten (FL |+| FR) (inr fra))
      ∎
  lscan-correct {A} (FL |x| FR) (fla , fra) = let
    (resfla , bonfla) = lscan FL fla
    (resfra , bonfra) = lscan FR fra
    l1 : bonfla ≡ snd (scan-list (flatten FL fla))
    l1 = cong snd (lscan-correct FL fla)
    l2 : bonfra ≡ snd (scan-list (flatten FR fra))
    l2 = cong snd (lscan-correct FR fra)
    l3 : (bonfla ∙_) ≡ (snd (scan-list (flatten FL fla)) ∙_)
    l3 = ext (λ { a → cong (_∙ a) l1 })
    l4 : flatten FR resfra ≡ fst (scan-list (flatten FR fra))
    l4 = cong fst (lscan-correct FR fra)
    l5 : flatten FR (map FR (bonfla ∙_) resfra) ≡
      map-list (snd (scan-list (flatten FL fla)) ∙_)
               (fst (scan-list (flatten FR fra)))
    l5 = begin
       flatten FR (map FR (bonfla ∙_) resfra)
      ≡⟨ map-flat-comm FR (bonfla ∙_) resfra ⟩
        map-list (bonfla ∙_) (flatten FR resfra)
      ≡⟨ cong₂ map-list l3 l4 ⟩
        map-list (snd (scan-list (flatten FL fla)) ∙_)
               (fst (scan-list (flatten FR fra)))
      ∎
    l6 : flatten FL resfla ≡ fst (scan-list (flatten FL fla))
    l6 = cong fst (lscan-correct FL fla)
    in begin
      (flatten FL resfla ++ flatten FR (map FR (bonfla ∙_) resfra))
      , bonfla ∙ bonfra
    ≡⟨ cong₂ _,_ (cong₂ _++_ l6 refl) refl ⟩
      (fst (scan-list (flatten FL fla))
      ++ flatten FR (map FR (bonfla ∙_) resfra)
      ) , bonfla ∙ bonfra
    ≡⟨ cong (λ x → (fst (scan-list (flatten FL fla)) ++ x)
                   , bonfla ∙ bonfra) l5 ⟩
      (fst (scan-list (flatten FL fla))
      ++ map-list (snd (scan-list (flatten FL fla)) ∙_)
                  (fst (scan-list (flatten FR fra)))
      ) , bonfla ∙ bonfra
    ≡⟨ cong₂ _,_ refl (cong₂ _∙_ l1 l2) ⟩
      (fst (scan-list (flatten FL fla))
      ++ map-list (snd (scan-list (flatten FL fla)) ∙_)
                  (fst (scan-list (flatten FR fra)))
      ) , snd (scan-list (flatten FL fla))
          ∙ snd (scan-list (flatten FR fra))
    ≡⟨ sym (scan-list-property (flatten FL fla) (flatten FR fra)) ⟩
      scan-list (flatten FL fla ++ flatten FR fra)
    ∎ where open HasMonoid {{...}}
  -- Proving the result for the case of concatenation of functors is
  -- outside the scope of the project, and therefore proving this
  -- correctness lemma leading up to the result is as well.
  lscan-correct (FL |∘| FR) flfra = sorry

  result : {A : Set} -> {{_ : HasMonoid A}} -> (F G : Functor) ->
    (fa : [ F ] A) -> (ga : [ G ] A) -> flatten F fa ≡ flatten G ga →
    first (flatten F) (lscan F fa) ≡ first (flatten G) (lscan G ga)
  result |Id| G a ga eqfaga = let
    l1 : first (_∷ []) (ε , a) ≡ ((ε ∷ []) , a)
    l1 = refl
    l2 : ((ε ∷ []) , a) ≡ scan-list (a ∷ [])
    l2 = sym (cong ((ε ∷ []) ,_) (proj₂ (IsMonoid.identity proof) a))
    l3 : scan-list (flatten G ga) ≡ first (flatten G) (lscan G ga)
    l3 = sym (lscan-correct G ga)
    in begin
    first (_∷ []) (ε , a)
      ≡⟨ l1 ⟩
    ((ε ∷ []) , a)
      ≡⟨ l2 ⟩
    scan-list (a ∷ [])
      ≡⟨ cong scan-list eqfaga ⟩
    scan-list (flatten G ga)
      ≡⟨ l3 ⟩
    first (flatten G) (lscan G ga)
      ∎
    where open HasMonoid {{...}}
  result |U| G tt ga eqfaga = begin
    first (λ tt → []) (tt , ε)
      ≡⟨ refl ⟩
    ([] , ε)
      ≡⟨ refl ⟩
    scan-list []
      ≡⟨ cong scan-list eqfaga ⟩
    scan-list (flatten G ga)
      ≡⟨ sym(lscan-correct G ga) ⟩
    first (flatten G) (lscan G ga)
      ∎
    where open HasMonoid {{...}}
  result (FL |+| FR) G (inl fla) ga eqfaga = begin
    first (flatten (FL |+| FR)) (first inl (lscan FL fla))
      ≡⟨ refl ⟩
    first (flatten FL) (lscan FL fla)
      ≡⟨ result FL G fla ga eqfaga ⟩
    first (flatten G) (lscan G ga)
      ∎
  result (FL |+| FR) G (inr fra) ga eqfaga = begin
    first (flatten (FL |+| FR)) (first inr (lscan FR fra))
      ≡⟨ refl ⟩
    first (flatten FR) (lscan FR fra)
      ≡⟨ result FR G fra ga eqfaga ⟩
    first (flatten G) (lscan G ga)
      ∎
  result (FL |x| FR) G (fla , fra) ga eqfaga = let
    (resfla , bonfla) = lscan FL fla
    (resfra , bonfra) = lscan FR fra
    in begin
      first (flatten (FL |x| FR))
        ((resfla , map FR (bonfla ∙_) resfra) , bonfla ∙ bonfra)
    ≡⟨ lscan-correct (FL |x| FR) (fla , fra) ⟩
      scan-list (flatten FL fla ++ flatten FR fra)
    ≡⟨ cong scan-list eqfaga ⟩
      scan-list (flatten G ga)
    ≡⟨ sym (lscan-correct G ga) ⟩
      first (flatten G) (lscan G ga)
    ∎
    where open HasMonoid {{...}}
  -- Proving the result for the case of concatenation of functors is
  -- outside the scope of the project.
  result (FL |∘| FR) G (flfra) ga eqfaga = sorry
